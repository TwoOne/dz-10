﻿#include <iostream>

int main()
{
    float a, b;
    std::cin >> a;
    std::cin >> b;
    std::cout << "Bigger number = " << std::max(a, b) << "\n";
    std::cout << "Their sum = " << a + b << "\n";
    std::cout << "Their multiplication = " << a * b << "\n";
    if (b == 0)
    {
        std::cout << "Can't divide by 0";
    }
    else
    {
        std::cout << "Their division = " << a / b << "\n";
    }

    /*std::cout << "Hello World!\n";*/
}

